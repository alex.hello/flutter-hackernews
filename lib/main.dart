import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:timeago/timeago.dart' as timeago;
import 'package:url_launcher/url_launcher.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'HN',
        debugShowCheckedModeBanner: false,
        home: SafeArea(
          child: Scaffold(
              appBar: PreferredSize(
                  preferredSize: Size.fromHeight(60), child: HNAppBar()),
              body: HNFeed()),
        ));
  }
}

class HNAppBar extends StatefulWidget {
  @override
  _HNAppBarState createState() => _HNAppBarState();
}

class _HNAppBarState extends State<HNAppBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromRGBO(255, 180, 78, 1.0),
      padding: EdgeInsets.all(6),
      child: Row(
        children: [
          Padding(
            padding: EdgeInsets.only(right: 20),
            child: IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {},
            ),
          ),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Historias principales",
                  softWrap: true,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
              Text("Última actualización: Hace 57 min",
                  softWrap: true,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: 16))
            ],
          )),
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.more_vert),
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}

class HNStoryData {
  int position;
  String title;
  String author;
  DateTime publishedAt;
  String url;
  int numPoints;
  int numComments;

  HNStoryData(
      {this.position,
      this.title,
      this.author,
      this.publishedAt,
      this.url,
      this.numPoints,
      this.numComments});

  factory HNStoryData.fromJson(Map<String, dynamic> data) {
    return HNStoryData(
      title: data["title"],
      author: data["by"],
      publishedAt: DateTime.fromMillisecondsSinceEpoch(data["time"] * 1000),
      url: data["url"],
      numPoints: data["score"],
      numComments: data["descendants"],
    );
  }
}

final HNStoryData exampleStory = HNStoryData(
    position: 1,
    title: "Funkwhale - Decentralized, self-hosted music server",
    author: "peterstensmyr",
    publishedAt: DateTime.now(),
    url: "funkwhale.audio",
    numPoints: 57,
    numComments: 36);

class HNFeed extends StatefulWidget {
  @override
  _HNFeedState createState() => _HNFeedState();
}

class ResourceIds {
  final List<String> ids;
  ResourceIds({this.ids});

  factory ResourceIds.fromJson(List<dynamic> values) {
    return ResourceIds(ids: values.map((k) => k.toString()).toList());
  }
}

class _HNFeedState extends State<HNFeed> {
  List<String> storiesIds = List();
  Map<String, HNStoryData> loadedStories = Map();

  @override
  Widget build(BuildContext context) {
    if (storiesIds.length == 0) {
      fetchTopStoriesIds().then((resourceIds) {
        storiesIds = resourceIds.ids;
      }).catchError((obj) => throw Exception(obj));
    }
    return ListView.builder(
        itemCount: storiesIds.length,
        itemBuilder: (context, index) {
          if (loadedStories.containsKey(storiesIds[index])) {
            return HNStory(storyData: loadedStories[storiesIds[index]]);
          } else {
            fetchStoryById(storiesIds[index]).then((story) {
              setState(() {
                loadedStories[storiesIds[index]] = story;
                loadedStories[storiesIds[index]].position = index + 1;
              });
            }).catchError(() => throw Exception("could not load story"));
            return Text("pending");
          }
        });
  }

  Future<ResourceIds> fetchTopStoriesIds() async {
    final response = await http.get(
        'https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty');
    if (response.statusCode == 200) {
      return ResourceIds.fromJson(jsonDecode(response.body));
    } else {
      throw Exception("Could not load top stories");
    }
  }

  Future<HNStoryData> fetchStoryById(String storyId) async {
    final response = await http.get(
        "https://hacker-news.firebaseio.com/v0/item/$storyId.json?print=pretty");
    if (response.statusCode == 200) {
      return HNStoryData.fromJson(jsonDecode(response.body));
    } else {
      throw Exception("Could not load story");
    }
  }
}

class HNStory extends StatefulWidget {
  HNStory({@required this.storyData});
  final HNStoryData storyData;

  @override
  _HNStoryState createState() => _HNStoryState();
}

class _HNStoryState extends State<HNStory> {
  @override
  Widget build(BuildContext context) {
    final String domain = Uri.parse(widget.storyData.url).host;
    return Container(
        child: Row(children: [
      Container(
        decoration: BoxDecoration(color: Color.fromARGB(255, 255, 223, 176)),
        padding: EdgeInsets.all(10),
        height: 90,
        width: 60,
        child: Column(
          children: [
            Text(widget.storyData.position.toString(),
                style: TextStyle(fontSize: 16)),
            Text("${widget.storyData.numPoints}p",
                style: TextStyle(color: Color.fromRGBO(253, 151, 6, 1.0))),
            Icon(Icons.local_fire_department,
                color: Color.fromRGBO(253, 151, 6, 1.0))
          ],
        ),
      ),
      Expanded(
          child: InkWell(
              onTap: () async {
                if (await canLaunch(widget.storyData.url)) {
                  await launch(widget.storyData.url);
                }
              },
              child: Container(
                decoration:
                    BoxDecoration(color: Color.fromRGBO(245, 245, 245, 1.0)),
                padding: EdgeInsets.all(10),
                height: 90,
                child: Column(
                  children: [
                    Align(
                        alignment: Alignment.topLeft,
                        child: Text(widget.storyData.title,
                            softWrap: true,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 16))),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Opacity(
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Align(
                                      child: Text(domain,
                                          softWrap: true,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              fontStyle: FontStyle.italic)),
                                      alignment: Alignment.topLeft),
                                  Align(
                                      child: Text(
                                        "${timeago.format(widget.storyData.publishedAt)} - ${widget.storyData.author}",
                                        softWrap: true,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      alignment: Alignment.topLeft)
                                ]),
                            opacity: 0.5),
                        Row(children: [
                          Row(children: [
                            IconButton(
                                icon: Icon(Icons.comment,
                                    color: Colors.redAccent)),
                            Text(widget.storyData.numComments.toString(),
                                style: TextStyle(
                                    color: Colors.redAccent, fontSize: 16))
                          ]),
                          SizedBox(width: 20),
                          IconButton(icon: Icon(Icons.more_vert))
                        ])
                      ],
                    )
                  ],
                ),
              ))),
    ]));
  }
}
